using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlusGold : MonoBehaviour
{
    private int Gold;
    public Text ContadorOro;
    // Start is called before the first frame update
    void Start()
    {
        Gold = 0;
    }

    public void Goldcounter()
    {
        Gold += 1;
        ContadorOro.text = Gold.ToString();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
