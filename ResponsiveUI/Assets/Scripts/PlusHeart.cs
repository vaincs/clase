using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlusHeart : MonoBehaviour
{
    private int Heart;
    public Text ContadorH;
    // Start is called before the first frame update
    void Start()
    {
        Heart = 100;
    }

    public void heartcounter()
    {
        Heart += 1;
        ContadorH.text = Heart.ToString();
    }
    // Update is called once per frame
    void Update()
    {

    }
}
